<?php
    require_once __DIR__ . '/config/CNCcalculatorConfig.php';
    require_once __DIR__ . '/backend/DataFromDatabase.php';
?>
<!doctype html>
<html>
<head>
    <title>Kalkulačka CNC</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,700;1,400&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/design.css?v=5">
</head>
<script>
    <?php
        // load materials data from database

        $dataFromDatabase = new DataFromDatabase();
        $dataFromDatabase->printMillingMaterialsArray();
        $dataFromDatabase->printDrillingMaterialsArray();
    ?>
</script>
<body <?php if(isset($_GET['hide_footer']) && $_GET['hide_footer'] == 'yes') echo 'class="hide-footer"'; ?>>

<script type="text/javascript" src="js/tabs.js?v=1"></script>
<script type="text/javascript" src="js/CNCcalculator.js?v=6"></script>

<script>
    var calculatorTabs = new tabs('frezovani');
    var calculator = new CNCcalculator(materialsMilling, materialsDrilling);
</script>

<div class="box">
    <div class="tab">
        <div class="tablinks" onclick="calculatorTabs.openTab(event, 'frezovani')" id="tab-frezovani">Frézování</div>
        <div class="tablinks" onclick="calculatorTabs.openTab(event, 'vrtani')" id="tab-vrtani">Vrtání</div>
    </div>

    <div id="frezovani" class="tabcontent">
        <form class="form" id="form" action="#" method="post" enctype="multipart/form-data">
            <h2>Nástroj</h2>
            <div class="w-50">
                <label>Průměr frézy</label>
                <input type="number" id="milling-diameter" min="0" max="12" onchange="calculator.refreshMillingChipLoad(); calculator.millingCalculate()">
                <span>mm</span>
            </div>
            <div class="w-50">
                <label>Počet zubů nástroje</label>
                <input type="number" id="milling-teeth" min="0" onchange="calculator.millingCalculate()">
            </div>
            <h2>Materiál</h2>
            <div style="margin-bottom: 15px">
                <select id="milling-material" onchange="calculator.refreshMillingChipLoad(); calculator.millingCalculate()">
                    <option value="0"></option>
                </select>
            </div>
            <div class="w-50">
                <label>Posuv na zub (chip load)</label>
            </div>
            <div class="w-50">
                <input type="number" id="milling-chipLoad" min="0" max="12" step="0.001" onchange="calculator.millingCalculate()">
                <span>mm</span>
            </div>
        </form>
        <div class="results">
            <h2>Otáčky (spindle speed)</h2>
            <div>
                <input disabled="" id="milling-spindleSpeed">
                <span>ot/min (RPM)</span>
            </div>

            <h2>Pracovní posuv (feed rate)</h2>
            <div>
                <input disabled="" id="milling-feedRate">
                <span>mm/min</span>
            </div>

            <h2>Zásuvový posuv (plunge rate)</h2>
            <div>
                <input disabled="" id="milling-plungeRate">
                <span>mm/min</span>
            </div>

            <h2>Max. hloubka průchodu (pass depth) <i class="hint fa-solid fa-circle-question"><span class="tooltiptext">Maximální možná hodnota je odvislá<br>od délky ostří nástroje</span></i></h2>
            <div>
                <input disabled="" id="milling-passDepth">
                <span>mm</span>
            </div>
        </div>
    </div>

    <div id="vrtani" class="tabcontent">
        <form class="form" id="form-vrtani" action="#" method="post" enctype="multipart/form-data">
            <h2>Nástroj</h2>
            <div>
                <label>Průměr vrtáku</label>
                <input type="number" id="drilling-diameter" min="3" max="10" onchange="calculator.calculateDrilling()">
                <span>mm</span>
            </div>
            <h2>Materiál</h2>
            <div class="w-100">
                <select id="drilling-material" onchange="calculator.calculateDrilling()">
                    <option value="0"></option>
                </select>
            </div>
        </form>
        <div class="results">
            <h2>Otáčky (spindle speed)</h2>
            <div>
                <input disabled="" id="drilling-spindleSpeed">
                <span>ot/min (RPM)</span>
            </div>

            <h2>Zásuvový posuv (plunge rate)</h2>
            <div>
                <input disabled="" id="drilling-plungeRate">
                <span>mm/min</span>
            </div>
        </div>
    </div>
</div>

<div id="footer" class="box">
    <div>
        <p>Kalklučka řezných podmínek přizpůsobena frézce <i>Jerabek Delta 210S (Syntec)</i></p>
        <p>Zdrojové kódy na <a href="https://gitlab.com/martinsykora.brno/cnc-calculator" target="_blank">GitLab</a></p>
        <p>Created by <a href="//fablabbrno.cz">FabLab Brno</a></p>
    </div>
</div>
<script>
    calculatorTabs.init();
    calculator.init();
</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</body>
</html>
