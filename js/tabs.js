class tabs {
    defaultTab = '';

    constructor(defaultTab) {
        self.defaultTab = defaultTab;
    }

    init() {
        var tabName = document.location.hash;
        if (tabName != '') {
            document.getElementById("tab-" + tabName.substr(10)).click();
        }
        else {
            document.getElementById("tab-" + self.defaultTab).click();
        }
    }

    openTab(evt, tabName) {
        var i, tabcontent, tablinks;

        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }

        document.getElementById(tabName).style.display = "block";
        evt.currentTarget.className += " active";

        document.location.hash = "#open-tab-" + tabName;
    }
}
