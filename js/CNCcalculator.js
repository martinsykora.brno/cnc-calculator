class CNCcalculator {
    /*  operating with materials in array in format

    var materialsMilling = [
        [
            'material name',
            t,
            vc,
            [0, chipLoad for diameter 1, diam 2, diam 3, diam 4, diam 5, diam 6, 0,  diam 8, 0, diam 10, 0, diam 12]
        ]
    ]

    var materialsMilling = [
        [
            'material name',
            t,
            vc,
            [0, 0, 0, chipLoad for diameter 3, diam 4, diam 5, diam 6, diam 7, diam 8, diam 9, diam 10]
        ]
    ]

     */

    materialsMilling = [];
    materialsDrilling = [];

    constructor(materialsMilling, materialsDrilling) {
        self.materialsMilling = materialsMilling;
        self.materialsDrilling = materialsDrilling;
    }

    refreshMillingChipLoad() {
        var diameter = parseInt(document.getElementById('milling-diameter').value);
        if(isNaN(diameter)) {
            diameter = 0;
        }
        var materialId = parseInt(document.getElementById('milling-material').value) - 1;

        if(materialId >= 0 && diameter !== 0) {
            var chipLoad = this.getMillingChipLoad(materialId, diameter);

            document.getElementById('milling-chipLoad').value = chipLoad;
        }
    }

    getMillingChipLoad(materialId, diameter) {
        var d = Math.round(diameter);

        // zaokrouhledni rozmezi 6 - 8
        if(diameter > 6 && diameter < 7) {
            d = 6
        }
        else if(diameter >= 7 && diameter < 8) {
            d = 8
        }
        // zaokrouhledni rozmezi 8 - 10
        else if(diameter > 8 && diameter < 9) {
            d = 8
        }
        else if(diameter >= 9 && diameter < 10) {
            d = 10
        }
        // zaokrouhledni rozmezi 10 - 12
        else if(diameter > 10 && diameter < 11) {
            d = 10
        }
        else if(diameter >= 11 && diameter < 12) {
            d = 12
        }

        return self.materialsMilling[materialId][3][d];
    }

    millingCalculate() {
        var diameter = parseInt(document.getElementById('milling-diameter').value);
        if(isNaN(diameter)) {
            diameter = 0;
        }
        var teeth = parseInt(document.getElementById('milling-teeth').value);
        if(isNaN(teeth)) {
            teeth = 0;
        }
        var materialId = parseInt(document.getElementById('milling-material').value) - 1;

        if(materialId >= 0 && diameter !== 0 && teeth !== 0) {
            var spindleSpeed = this.calculateMillingSpindleSpeed(diameter, materialId);
            var feedRate = this.calculateMillingFeedRate(spindleSpeed, diameter, teeth, materialId);
            var plungeRate = this.calculateMillingPlungeRate(feedRate);
            var passDepth = this.calculateMillingPassDepth(diameter, materialId);

            document.getElementById('milling-spindleSpeed').value = spindleSpeed;
            document.getElementById('milling-feedRate').value = feedRate;
            document.getElementById('milling-plungeRate').value = plungeRate;
            document.getElementById('milling-passDepth').value = passDepth;
        }
    }

    calculateMillingSpindleSpeed(diameter, materialId) {
        var vc = self.materialsMilling[materialId][2];
        var n = Math.round((vc * 1000)/(3.1415*diameter));

        if(n > 24000) {
            n = 24000;
        }
        else if (n < 12000) {
            n = 12000;
        }

        return n;
    }

    calculateMillingFeedRate(spindleSpeed, diameter, teeth, materialId) {
        var fz = document.getElementById('milling-chipLoad').value;

        var feedRate = Math.round(spindleSpeed * teeth * fz);

        return feedRate > 10000 ? 10000 : feedRate;
    }

    calculateMillingPlungeRate(feedRate) {
        return Math.round(feedRate * 0.3);
    }

    calculateMillingPassDepth(diameter, materialId) {
        var t = self.materialsMilling[materialId][1];

        return diameter * t;
    }

    calculateDrilling() {
        var diameter = parseInt(document.getElementById('drilling-diameter').value);
        if(isNaN(diameter)) {
            diameter = 0;
        }

        var materialId = parseInt(document.getElementById('drilling-material').value) - 1;

        if(materialId >= 0 && diameter !== 0) {
            var spindleSpeed = this.calculateDrillingSpindleSpeed(diameter, materialId);
            var plungeRate = this.calculateDrillingPlungeRate(spindleSpeed, diameter, materialId);

            document.getElementById('drilling-spindleSpeed').value = spindleSpeed;
            document.getElementById('drilling-plungeRate').value = plungeRate;
        }
    }

    calculateDrillingSpindleSpeed(diameter, materialId) {
        var vc = self.materialsDrilling[materialId][1];
        var n = Math.round((vc * 1000)/(3.1415*diameter));

        return n;
    }

    calculateDrillingPlungeRate(spindleSpeed, diameter, materialId) {
        return Math.round(spindleSpeed * 2 * self.materialsDrilling[materialId][2][Math.round(diameter)]);
    }

    init() {
        this.initMillingMaterials();
        this.initDrillingMaterials();
    }

    //load milling materials to select in form
    initMillingMaterials() {
        for(var i = 1; i <= self.materialsMilling.length; i++) {
            document.getElementById('milling-material').innerHTML += '<option value="' + i + '">' + self.materialsMilling[i - 1][0] + '</option>';
        }
    }

    //load drilling materials to select in form
    initDrillingMaterials() {
        for(var i = 1; i <= self.materialsDrilling.length; i++) {
            document.getElementById('drilling-material').innerHTML += '<option value="' + i + '">' + self.materialsDrilling[i - 1][0] + '</option>';
        }
    }
}
