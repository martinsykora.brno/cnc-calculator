<?php
$mysqli = new mysqli(
    'md202.wedos.net',
    'a57773_flkalk',
    'gGVPcADE',
    'd57773_flkalk');
if ($mysqli->connect_error)
    echo 'Nelze se připojit k databázi';
$mysqli->set_charset('utf8');

// materials milling

$sql = $mysqli->query("SELECT * FROM materials_milling ORDER BY `order`");
echo 'var materialsMilling = [';
if($sql->num_rows) {
    $arr = '';
    while($item = $sql->fetch_array()) {
        $arr .= '
            [
                \'' . $item['name'] . '\',
                ' . $item['t'] . ',
                ' . $item['vc'] . ',
                [0,';
        for($i = 1; $i <= 11; $i++) {
            $arr .= $item['chipload_d_' . $i] . ',';
        }
        $arr .= $item['chipload_d_12'] .
            ']
            ],';
    }
    echo substr($arr, 0, -1);
}
echo '];';

// materials drilling

$sql = $mysqli->query("SELECT * FROM materials_drilling ORDER BY `order`");
echo 'var materialsDrilling = [';
if($sql->num_rows) {
    $arr = '';
    while($item = $sql->fetch_array()) {
        $arr .= '
            [
                \'' . $item['name'] . '\',
                ' . $item['vc'] . ',
                [0,';
        for($i = 1; $i <= 9; $i++) {
            $arr .= $item['chipload_d_' . $i] . ',';
        }
        $arr .= $item['chipload_d_10'] .
            ']
            ],';
    }
    echo substr($arr, 0, -1);
}
echo '];';
