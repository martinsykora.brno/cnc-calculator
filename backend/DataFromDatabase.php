<?php

class DataFromDatabase {
    private $mysqli;

    public function __construct() {
        $this->connectToDatabase();
    }

    private function connectToDatabase() {
        $this->mysqli = new mysqli(
            CNCcalculatorConfig::DB_SERVER,
            CNCcalculatorConfig::DB_USERNAME,
            CNCcalculatorConfig::DB_PASSWORD,
            CNCcalculatorConfig::DB_NAME
        );
        if ($this->mysqli->connect_error)
            echo 'Nelze se připojit k databázi';
        $this->mysqli->set_charset('utf8');
    }

    public function printMillingMaterialsArray() {
        $sql = $this->mysqli->query("SELECT * FROM materials_milling ORDER BY `order`");
        $array = 'var materialsMilling = [';
        if($sql->num_rows) {
            while($item = $sql->fetch_array()) {
                $array .= '
            [
                \'' . $item['name'] . '\',
                ' . $item['t'] . ',
                ' . $item['vc'] . ',
                [0,';
                for($i = 1; $i <= 11; $i++) {
                    $array .= $item['chipload_d_' . $i] . ',';
                }
                $array .= $item['chipload_d_12'] .
                    ']
            ],';
            }
            $array = substr($array, 0, -1);
        }
        $array .= '];';

        echo $array;
    }

    public function printDrillingMaterialsArray() {
        $sql = $this->mysqli->query("SELECT * FROM materials_drilling ORDER BY `order`");
        $array = 'var materialsDrilling = [';
        if($sql->num_rows) {
            while($item = $sql->fetch_array()) {
                $array .= '
                [
                    \'' . $item['name'] . '\',
                    ' . $item['vc'] . ',
                    [0,';
                    for($i = 1; $i <= 9; $i++) {
                        $array .= $item['chipload_d_' . $i] . ',';
                    }
                $array .= $item['chipload_d_10'] .
                        ']
                ],';
            }
            $array = substr($array, 0, -1);
        }
        $array .= '];';

        echo $array;
    }
}
